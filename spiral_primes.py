import math
import time
start_time = time.time()

def is_prime(n):
	if n <= 1:
		return False
	if n == 2:
		return True
	if n > 2 and n % 2 == 0:
		return False
	max_div = math.floor(math.sqrt(n))
	for i in range(3, 1 + max_div, 2):
		if n % i == 0:
			return False
	return True

def numbers_spiral_diagonals():
    last_num = 0
    count = 1
    count_prime = 0
    i = 0
    while True:
        for j in range(1,5):
            if is_prime(last_num + 1 + i*2*j):
                count_prime += 1
            count += 1
        last_num +=  i*2*j
        i += 1
        if i > 3 and count_prime/count < 0.1:
            return (i*2) + 1

print(numbers_spiral_diagonals())
print(f"--- {(time.time() - start_time):.10f} seconds ---" )